package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	 "sync"
	// "time"

	"github.com/joho/godotenv"
)

var wg sync.WaitGroup
type Data struct {
	Continent      string `json:"continent"`
	Country        string `json:"country"`
	Population     string `json:"population"`
}


func APIdata(val string , key string) {

	// url := "https://corona-virus-world-and-india-data.p.rapidapi.com/api"
	url := "https://covid-193.p.rapidapi.com/statistics"

	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("x-rapidapi-host", "covid-193.p.rapidapi.com")
	req.Header.Add("x-rapidapi-key", key)

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		panic(err)
	}

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	jsonString := string(body)

	var result map[string]interface{}
	json.Unmarshal([]byte(jsonString), &result)

	var data Data
	// population := make(chan string)
	// timeUp := time.After(time.Second *time.Duration(Time))

	// 
	//   wg.Add(2)

	//    msg := make(chan string)
	    for _, item := range result["response"].([]interface{}) {
				  
		    data.Continent = fmt.Sprintf("%s", item.(map[string]interface{})["continent"])
		    data.Country = fmt.Sprintf("%s", item.(map[string]interface{})["country"])
		    data.Population = fmt.Sprintf("%f", item.(map[string]interface{})["population"])
	
			//  msg <- "JSON data written"
			
		jsonData, err := json.MarshalIndent(data, "", " ")
		if err != nil {
			panic(err)
		}

		f, err := os.OpenFile("populationData.json", os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0644)

		// O_APPEND It appends data to the file when writing
		// O_WRONLY It opens the file write-only
		// O_CREATE It creates a new file if none exists

		if err != nil {
			panic(err)
		}
		defer f.Close()

		if _, err = f.Write(jsonData); err != nil {
			panic(err)
		}
		// wg.Done()
	// wg.Wait()
}
	// wg.Done()
 
//  v := <- msg
	fmt.Println("JSON data written")
	// fmt.Println(v)
}

func main() {
    godotenv.Load()
    csvFilename := flag.String("csv", "country.csv", "file containing list of countries for population status")
    flag.Parse()

    file, err := os.Open(*csvFilename)
    if err != nil {
        exit(fmt.Sprintf("failed to open the csv file %s\n", *csvFilename))
    }
    //r := csv.NewReader(file)
    key := os.Getenv("API_KEY")
    scanner := bufio.NewScanner(file)
    scanner.Split(bufio.ScanLines)
    var list []string

    for scanner.Scan() {
        list = append(list, strings.ToUpper(scanner.Text()))
    }
    for i, country := range list {
        fmt.Printf("%d. %s\n", i+1, country)
    }
    fmt.Println("Enter country name:")
    var val string
    fmt.Scanf("%s", &val)
    val = strings.ToUpper(val)
    countryFound := find(list, val)
    country := make(chan bool)
    wg.Add(2)
    go func() {
    if countryFound {
        fmt.Printf("Country %s found. Data fetching please wait...\n", val)
     go APIdata(val, key)
     <- country
    } else {
        fmt.Printf("Country %s not found. Please enter from list above", val)
    }
    wg.Done()
    }()
    wg.Wait()
    
}

// func find(list []string, val string) {
// 	panic("unimplemented")
// }

func exit(s string) {
	panic("unimplemented")
}

func find(countries []string, val string) bool {
    for i := range countries {
        if countries[i] == val {
            return true
        }
    }
    return false
}
